﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Patrones.Flyweight
{
    class Program
    {
        static void Main(string[] args)
        {
            FlyweighFactoryCaracter factory = new FlyweighFactoryCaracter();
            string documento = "ABCABCABCABC";
            char[] caracteres = documento.ToCharArray();
            int tamanio = 10;
            
            foreach (var item in caracteres)
            {
                factory.GetCaracter(item).Mostrar(tamanio);
            }

            Console.WriteLine("Total de letras: {0}", caracteres.Count());
            Console.WriteLine("Total de objetos: {0}", factory.GetCount());
            Console.ReadKey();
        }
        
    }

}
