﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Patrones.Flyweight
{
    public abstract class FlyweightCaracter
    {
        protected int tamanio;
        protected char simbolo;
        public abstract void Mostrar(int tamanio);
    }
}


