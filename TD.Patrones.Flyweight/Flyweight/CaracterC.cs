﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Patrones.Flyweight
{
    public class CaracterC : FlyweightCaracter
    {
        public CaracterC()
        {
            this.simbolo = 'C';
        }
        public override void Mostrar(int tamanio)
        {
            this.tamanio = tamanio;
            Console.WriteLine(String.Format("Simbolo {0} tamaño {1}", this.simbolo, this.tamanio));
        }
    }
}
