﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Patrones.Flyweight
{
    public class FlyweighFactoryCaracter
    {
        private Dictionary<char, FlyweightCaracter> caracteres = new Dictionary<char, FlyweightCaracter>();


        public int GetCount()
        {
            return caracteres.Count();
        }
        public FlyweightCaracter GetCaracter(char key)
        {
            FlyweightCaracter caracter = null;
                
            if (caracteres.ContainsKey(key))
                caracter = caracteres[key];
            else
            {
                switch (key)
                {
                    case 'A': caracter = new CaracterA(); break;
                    case 'B': caracter = new CaracterB(); break;
                    case 'C': caracter = new CaracterC(); break;
                }
                caracteres.Add(key, caracter);
            }
            return caracter;
        }
    }
}
